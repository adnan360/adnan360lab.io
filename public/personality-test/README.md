# Personality Test

This is a personality test in Bangla language written in HTML, CSS, Javascript. It was originally written for mobile Cordova app. It asks you some yes-no questions and then calculates your personality. It also gives you your appropriate career based on your personality.

__CAUTION:__ The results of this test is not supposed to be 100% accurate, but in my tests of different people I saw it to be around 90% accurate. This is made for educational purpose and not for business. The author(s) of this will not take any responsibility of any loss caused by this. Use at your own risk.

To see screenshots, check the `_Screenshots` directory.


এটা একটা ব্যক্তিত্ব পরীক্ষার সফটওয়্যার যেটা HTML, CSS এবং Javascript দিয়ে বানানো হয়েছে। এটা Cordova মোবাইল এ্যাপ তৈরির জন্য লেখা হয়েছিল। তবে এটা স্ট্যাটিক পেজ হিসেবে ব্রাউজারেও ব্যবহার করা যায়। এটা কয়েকটা হ্যাঁ-না প্রশ্ন জিজ্ঞেস করে এবং সেই উত্তরের উপর ভিত্তি করে যে কারো ব্যক্তিত্ব নির্ণয় করতে পারে। এটা আবার এই ব্যক্তিত্বের উপর ভিত্তি করে আপনার উপযোগী ক্যারিয়ারও দেখাতে পারে।

__সতর্কতা:__ এটার ফলাফল ১০০% সঠিক না-ও হতে পারে, কিন্তু আমি যতজনকে পরীক্ষা করেছি তাতে ৯০% মিল পেয়েছি। এটা শুধু লেখাপড়ার কাজে ব্যবহারের জন্য তৈরি হয়েছে, ব্যবসায়িক কাজের ব্যবহারের জন্য নয়। সফটওয়্যারটির লেখক(বৃন্দ) এর মাধ্যমে কোন ক্ষতির জন্য দায়ী থাকবেন না। নিজ দায়িত্বে ব্যবহার করুন।

স্ক্রিনশট দেখার জন্য `_Screenshots` ডিরেক্টরি দেখতে পারেন।

License: GPLv2