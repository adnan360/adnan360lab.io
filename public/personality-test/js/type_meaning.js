var type_meaning =
{
	"I":"Introverted",
	"E":"Extroverted",
	
	"S":"Sensing",
	"N":"Intuition",
	
	"T":"Thinking",
	"F":"Feeling",
	
	"J":"Judging",
	"P":"Perceiving"
};