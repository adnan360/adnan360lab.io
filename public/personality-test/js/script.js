function banglaNumber(theNumber) {
	str = theNumber.toString();
	var array = {
		"0":"০",
		"1":"১",
		"2":"২",
		"3":"৩",
		"4":"৪",
		"5":"৫",
		"6":"৬",
		"7":"৭",
		"8":"৮",
		"9":"৯"
	};

	for (var val in array) {
		str = str.split(val).join(array[val]);
	}

	return str;
}

var qs = (function(a) {
	if (a == "") return {};
	var b = {};
	for (var i = 0; i < a.length; ++i)
	{
		var p=a[i].split('=', 2);
		if (p.length == 1)
			b[p[0]] = "";
		else
			b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
	}
	return b;
})(window.location.search.substr(1).split('&'));